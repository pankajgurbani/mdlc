[TOC]

# USAGE

This guide demonstrates the usage of each MDC component. All the components are made in a way that imitates the base lightning components, so if you have basic knowledge of how their corresponding lightning components work, you won't require much know-how on these.

Sample code snippets of each MDC component are given below as an example.

## MDCInputField

![picture](assets/MDCInputField.JPG)

The MDCInputField works with most of the html input types except the ones which are provided as a seperate component (eg: MDCInputFile, MDCCheckboxGroup, MDCRadioButton)

### Examples:

    <div>
        <c:MDCInputField
            aura:id="mdc-input-field"
            value="{!v.name}"
            label="Name"
            helptext="Enter your name here"
            messageWhenValueMissing="You must enter your name"
            placeholder="Eg. Thomas"
            maxlength="20"
            required="true"
        />
    </div>

    <div>
        <c:MDCInputField
            aura:id="mdc-input-field"
            value="{!v.email}"
            label="Email"
            helptext="Enter your email here"
            pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}"
            messageWhenPatternMismatch="Enter a valid email address"
            placeholder="Eg. shelbythomas@peakyblinders.com"
            required="true"
        />
    </div>

    <div>
        <c:MDCInputField
        aura:id="mdc-input-field"
        value="{!v.date}"
        label="Select a date"
        type="date"
        helptext="Enter your birthdate"
        />
    </div>

MDCInputField also works with types: **date, time, color, password, etc**

## MDCCheckboxGroup

![picture](assets/MDCCheckboxGroup.JPG)

The MDCCheckboxGroup displays a group of checkboxes. The values of selected checkboxes will be in the form of a string array.

### Example:

    <aura:attribute
        name="checkboxOptions"
        type="List"
        default="[{'label': 'Awesome', 'value': 'awesome'},{'label': 'Pretty Cool!', 'value': 'prettycool'},{'label': 'Amazing', 'value': 'amazing'},{'label': 'Super duper fantastic', 'value': 'superfantastic'}]"
    />
    <aura:attribute name="checkboxValue" type="String[]" />

    <span class="label">Material Design Components are: </span>
    <c:MDCCheckboxGroup
        aura:id="mdc-input-field"
        name="checkboxgroup"
        options="{!v.checkboxOptions}"
        value="{!v.checkboxValue}"
        required="false"
    />

## MDCRadioGroup

![picture](assets/MDCRadioGroup.JPG)

The MDCRadioGroup displays a group of radio buttons. The value of the selected radio button is of string datatype

### Example:

    <aura:attribute
        name="radioOptions"
        type="List"
        default="[{'label': 'Einstein', 'value': 'einstein'},{'label': 'Google', 'value': 'google'},{'label': 'Chickens', 'value': 'chickens'},{'label': 'Space People', 'value': 'spacepeople'}]"
    />
    <aura:attribute name="radioValue" type="String" />

    <span class="label">Material Design Components are maintained by:</span>
    <c:MDCRadioGroup
        aura:id="mdc-input-field"
        options="{!v.radioOptions}"
        name="radiogroup"
        value="{!v.radioValue}"
        otherOptionValue="{!v.radioValue}"
        required="false"
    />

## MDCSelectMenu

![picture](assets/MDCSelectMenu.JPG)

The MDCSelectMenu displays a dropdown list of options to select from. Only a single option can be selected.

### Example:

    <aura:attribute
        name="selectOptions"
        type="List"
        default="[{'label': 'A web framework', 'value': 'framework'},{'label': 'A gold machine', 'value': 'goldmachine'},{'label': 'A cloud', 'value': 'cloud'},{'label': 'A coffee', 'value': 'coffee'}]"
    />
    <aura:attribute name="selectValue" type="String" />

    <div>
        <c:MDCSelectMenu
            aura:id="mdc-field-input"
            value="{!v.selectValue}"
            required="true"
            options="{!v.selectOptions}"
            label="Material Design is"
            helptext="What is material design?"
        />
    </div>

## MDCTextArea

![picture](assets/MDCTextArea.JPG)

The MDCTextArea is . . . you know what it is.

### Example:

    <div>
        <c:MDCTextArea
            aura:id="field-input"
            placeholder="Long-answer text"
            required="true"
            maxlength="200"
            label="What are your thoughts on the material design components?"
            helptext="Describe in detail"
        />
    </div>

## MDCInputFile

![picture](assets/MDCInputFile.JPG)

The MDCInputFile component is used to select a local file.

### Example:

    <div>
        <c:MDCInputFile
          aura:id="mdc-field-input"
          label="Upload File"
          value="{!v.value}"
          required="false"
          maxSize="1"
          maxNumberOfFiles="2"
        />
    </div>

## MDCButton

![picture](assets/MDCButtons.JPG)

The MDCButton has the following types: **default, outlined, raised, unelevated**

### Example:

    <span class="slds-var-m-around_x-small">
        <c:MDCButton
            label="Submit"
            type="default"
        />
    </span>
    <span class="slds-var-m-around_x-small">
        <c:MDCButton
            label="Submit"
            type="outlined"
        />
    </span>
    <span class="slds-var-m-around_x-small">
        <c:MDCButton
            label="Submit"
            type="raised"
        />
    </span>
    <span class="slds-var-m-around_x-small">
        <c:MDCButton
            label="Submit"
            type="unelevated"
        />
    </span>

## MDCCard

![picture](assets/MDCCard.JPG)

The MDCCard component comes with useful inner components: **MDCCardMedia, MDCCardActionButton**.  
It has 2 variants: **default, outlined**

### Example:

    <c:MDCCard variant="default" class="slds-var-p-around_small">
        <!-- Use primaryAction facet if the majority of the body contains an action area -->
        <aura:set attribute="primaryAction">
            <!-- A rich media container -->
            <!-- Add background-image to 'image-bg' class to add background image to the container -->
            <c:MDCCardMedia
                shape="rect"
                class="image-bg"
                contentClass="slds-align_absolute-center"
                onclick="{!c.onClick}"
            >
                This is some text on image
            </c:MDCCardMedia>
        </aura:set>

        <!-- Body of the card -->
        <div class="slds-var-p-around_small slds-text-align_center">
            This is some text here
        </div>

        <aura:set attribute="actions">
            <!-- Action buttons -->
            <c:MDCCardActionButton onclick="{!c.onClick}">
                Cancel
            </c:MDCCardActionButton>
            <c:MDCCardActionButton onclick="{!c.onClick}">
                Done
            </c:MDCCardActionButton>
        </aura:set>
    </c:MDCCard>

# FAQ

## How to check/report a component's validity?

To check and report a component's validity you must call the component's `reportValidity()` method. This method returns true if the component is valid or false otherwise, it also displays the validation message on the component.

### Example:
    ({
        onclick: function(component, event, helper) {
            var valid = true;
            component.find('mdc-input-field').forEach(
                cmp => {
                    valid = valid && cmp.reportValidity();
                }
            );
            if(valid) {
                // your code here
            }
        }
    })
