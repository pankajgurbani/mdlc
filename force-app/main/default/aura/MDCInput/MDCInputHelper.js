({
  setValidationMessage: function (component, el) {
    var val = el.validity;
    var valMsg;
    if (!val.valid) {
      if (val.patternMismatch) {
        valMsg = component.get("v.messageWhenPatternMismatch");
      } else if (val.valueMissing) {
        valMsg = component.get("v.messageWhenValueMissing");
      } else if (val.badInput) {
        valMsg = component.get("v.messageWhenBadInput");
      } else if (val.tooLong) {
        valMsg = component.get("v.messageWhenValueTooLong");
      } else if (val.tooShort) {
        valMsg = component.get("v.messageWhenValueTooShort");
      } else if (val.rangeOverflow || val.rangeUnderflow) {
        valMsg = component.get("v.messageWhenValueOutOfRange");
      } else if (val.customError) {
        valMsg = component.get("v.validationMessage");
      }
    }
    component.set("v.validationMessage", valMsg);
  },

  reportValidity: function (element, noFocus) {
    element.focus();
    element.blur();
    if(!noFocus) {
        element.focus();
    }
  },

  processDynamicStyles: function (component) {
    this.processLabelClasses(component);
  },

  processLabelClasses: function (component) {
    var label = component.get("v.label");
    var disabled = component.get("v.disabled");
    var labelEl = component.find("mdc-label");
    if (labelEl) labelEl = labelEl.getElement();
    else return;

    if (!label) {
      labelEl.classList.add("mdc-text-field--no-label");
    } else {
      labelEl.classList.remove("mdc-text-field--no-label");
    }

    if (disabled === true) {
      labelEl.classList.add("mdc-text-field--disabled");
    } else {
      labelEl.classList.remove("mdc-text-field--disabled");
    }
  }
});