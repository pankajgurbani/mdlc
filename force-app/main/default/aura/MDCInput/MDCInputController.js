({
  onReportValidity: function (component, event, helper) {
    var params = event.getParam('arguments');
    var el = component.find("mdc-cmp");
    if(!el) return true;

    if (component.get("v.type") == "date") {
      return el.reportValidity();
    }
    
    el = el.getElement();

    var validationMessage = params.customValidity;
    if(validationMessage !== null && validationMessage !== undefined) {
        component.set('v.validationMessage', validationMessage);
        el.setCustomValidity(validationMessage);
    }

    if (!el.validity.valid) {
      helper.reportValidity(el, params.noFocus);
    }
    return el.validity.valid;
  },

  onblur: function (component, event, helper) {
    helper.setValidationMessage(component, component.find("mdc-cmp").getElement());
    helper.fireMDCEvent(component, event, 'onblur', 'blur');
  },

  onfocus: function (component, event, helper) {
    helper.fireMDCEvent(component, event, 'onfocus', 'focus');
  },

  onchange: function(component, event, helper) {
    helper.fireMDCEvent(component, event, 'onchange', 'change');
  },

  oninput: function (component, event, helper) {
    component.set("v.value", event.target.value);
    helper.fireMDCEvent(component, event, 'oninput', 'input');
  },

  ondatechange: function (component, event, helper) {
    component.set("v.value", event.getParam("value"));
  },
  
  ondateblur: function (component, event, helper) {
    helper.setValidationMessage(component, event.getParam("target"));
    let mdcDateCmp = component.find("mdc-cmp");
    if (mdcDateCmp) {
      mdcDateCmp.setCustomValidity(component.get("v.validationMessage"));
    }
  }
});