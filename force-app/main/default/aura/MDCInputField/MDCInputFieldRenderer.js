({
	afterRender: function(component, helper) {
        this.superAfterRender();
        helper.processDynamicStyles(component);
    },
    
    rerender: function(component, helper) {
        this.superRerender();
        helper.processDynamicStyles(component);
    }
})