({
  uploadFiles: function (component, files) {
    if (!files || !files.length) return;

    var uploadedFiles = component.get("v.value");
    if (!Array.isArray(uploadedFiles)) uploadedFiles = [];

    var maxSize = component.get("v.maxSize");

    var totalSize = component.get("v.totalSize");
    var totalNumberOfFiles = component.get("v.totalNumberOfFiles");

    if (
      !this.validateNumberOfFiles(component, totalNumberOfFiles + files.length)
    )
      return;

    for (var i = 0; i < files.length; i++) {
      var file = files[i];
      var fileSizeInMb = file.size / (1024 * 1024);
      totalSize += fileSizeInMb;

      if (!this.validateSize(component, totalSize, maxSize)) return;

      let fileReader = new FileReader();
      fileReader.addEventListener(
        "load",
        $A.getCallback(() => {
          let fileContents = fileReader.result;
          uploadedFiles.push({
            fileContents: fileContents,
            name: file.name,
            type: file.type,
            size: file.size
          });
          component.set("v.value", uploadedFiles);
        })
      );
      fileReader.readAsDataURL(file);
    }
  },
  setValidity: function (component, message) {
    component.set("v.validationMessage", message);
  },
  validateRequired: function (component) {
    var files = component.get("v.value");
    if (component.get("v.required")) {
      if (!Array.isArray(files) || !files.length) {
        this.setValidity(component, component.get("v.messageWhenValueMissing"));
        this.resetFileUploadValue(component);
        return false;
      }
    }
    this.setValidity(component, null);
    return true;
  },
  validateNumberOfFiles: function (component, totalNumberOfFiles) {
    var uploadedFiles = component.get("v.value");
    var maxNumberOfFiles = component.get("v.maxNumberOfFiles");
    if (
      uploadedFiles &&
      this.isNumber(maxNumberOfFiles) &&
      totalNumberOfFiles > maxNumberOfFiles
    ) {
      this.setValidity(component, "Max files allowed are " + maxNumberOfFiles);
      this.resetFileUploadValue(component);
      return false;
    } else {
      this.setValidity(component, null);
      return true;
    }
  },
  validateSize: function (component, totalSize, maxSize) {
    if (this.isNumber(maxSize) && totalSize > maxSize) {
      this.setValidity(component, "Max file size limit is " + maxSize + "MB");
      this.resetFileUploadValue(component);
      return false;
    } else {
      this.setValidity(component, null);
      return true;
    }
  },
  resetFileUploadValue: function (component) {
    var input = component.find("fileUpload");
    if (input) input.getElement().value = "";
  },
  isNumber: function (num) {
    return !isNaN(num) && num !== null;
  }
});