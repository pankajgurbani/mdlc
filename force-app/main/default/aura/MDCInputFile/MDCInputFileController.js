({
  onFileUpload: function (component, event, helper) {
    let files = event.target.files;
    helper.uploadFiles(component, files);
    helper.fireMDCEvent(component, event, "onchange", "change");
  },

  onValueChange: function (component, event, helper) {
    var files = component.get("v.value");
    if (!Array.isArray(files)) {
      component.set("v.totalSize", 0);
      component.set("v.totalNumberOfFiles", 0);
      return;
    }
    component.set("v.totalNumberOfFiles", files.length);
    var totalSize = 0;
    for (var i = 0; i < files.length; i++) {
      totalSize += files[0].size / (1024 * 1024);
    }
    component.set("v.totalSize", totalSize);

    helper.validateRequired(component) &&
      helper.validateNumberOfFiles(component, files.length) &&
      helper.validateSize(component, totalSize, component.get("v.maxSize"));
  },

  onReportValidity: function (component, event, helper) {
    var totalFiles = component.get("v.totalNumberOfFiles");
    var valid = 
      helper.validateRequired(component) &&
      helper.validateNumberOfFiles(component, totalFiles) &&
      helper.validateSize(component, totalFiles, component.get("v.maxSize"));
    if(!valid) {
        var fileUpload = component.find('fileUpload');
        fileUpload && fileUpload.getElement && fileUpload.getElement.focus && fileUpload.getElement.focus();
    }
    return valid;
  }
});