({
  render: function (component, helper) {
    var ret = this.superRender();
    helper.renderSVG(component);
    return ret;
  }
});