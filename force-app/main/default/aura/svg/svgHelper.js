({
  renderSVG: function (component) {
    var container = component.find("container");
    if (!container) return;
    container = container.getElement();
    var svgns = "http://www.w3.org/2000/svg";

    var svg = document.createElementNS(svgns, "svg");
    container.appendChild(svg);
    svg.setAttribute("class", component.get("v.svgClass"));
    svg.setAttribute("viewBox", component.get("v.viewBox"));

    if (component.get("v.path")) {
      var path = document.createElementNS(svgns, "path");
      svg.appendChild(path);
      path.setAttribute("class", component.get("v.pathClass"));
      path.setAttribute("fill", component.get("v.fill"));
      path.setAttribute("d", component.get("v.path"));
    }
    var polygons = component.get("v.polygons");
    if (!Array.isArray(polygons)) polygons = [];
    polygons.forEach((poly) => {
      let polyEl = document.createElementNS(svgns, "polygon");
      svg.appendChild(polyEl);
      polyEl.setAttribute("class", poly.class);
      polyEl.setAttribute("stroke", poly.stroke);
      polyEl.setAttribute("fill-rule", poly.fillRule);
      polyEl.setAttribute("points", poly.points);
    });
  }
});