({

    afterRender: function(component, helper) {
        var render = this.superAfterRender();
        try {
            helper.processDynamicClasses(component);
        } catch(err) {}
        return render;
    },

    rerender: function(component, helper) {
        this.superRerender();
        try {
            helper.processDynamicClasses(component);
        } catch(err) {}
    }

})