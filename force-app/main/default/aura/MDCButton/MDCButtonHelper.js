({
	processDynamicClasses : function (component) {
		var btn = component.find('mdc-btn');
		if(!btn) return;
		btn = btn.getElement();

		var type = component.get('v.type');
		btn.className = 'mdc-button';
		if(type === 'outlined') {
			btn.classList.add('mdc-button--outlined');
		} else if(type === 'raised') {
			btn.classList.add('mdc-button--raised');
		} else if(type === 'unelevated') {
			btn.classList.add('mdc-button--unelevated');
		}
	}
})