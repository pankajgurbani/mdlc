({
	onReportValidity : function(component, event, helper) {
		return helper.isValid(component);
	}
})