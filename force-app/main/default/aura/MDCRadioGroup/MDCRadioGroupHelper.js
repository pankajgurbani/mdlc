({
	isValid : function(component) {
        var valid = this.isChoicesValid(component);
        var otherOpt = component.find('field-input');
        if(otherOpt) {
            valid = valid && otherOpt.reportValidity();
        }
        return valid;
	}
})