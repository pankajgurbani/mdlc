({
    onMaterialLoaded : function(component, event, helper) {
        helper.onRender(component);
    },
    onVariantChange : function(component, event, helper) {
        helper.setVariant(component);
    } 
})