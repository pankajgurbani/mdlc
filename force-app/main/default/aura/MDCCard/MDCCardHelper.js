({
  variantClasses: {
    outlined: "mdc-card mdc-card--outlined",
    default: "mdc-card"
  },
  onRender: function (component) {
    this.instantiate(component);
    this.setVariant(component);
  },
  setVariant: function (component) {
    try {
      var baseClass = component.get("v.class");
      var variant = component.get("v.variant");
      var mdcCard = this.getComponentElement(component, "mdc-card");
      if (!mdcCard) return;
      var cls = this.variantClasses[variant];
      if (cls) {
        mdcCard.setAttribute("class", cls + " " + baseClass);
      }
    } catch (err) {}
  },
  instantiate: function (component) {
    try {
      const selector = ".mdc-button, .mdc-icon-button, .mdc-card__primary-action";
      const ripples = [].map.call(
        document.querySelectorAll(selector),
        function (el) {
          mdc.ripple.MDCRipple.attachTo(el);
        }
      );
    } catch (err) {}
  }
});