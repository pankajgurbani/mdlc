({

    afterRender: function(component, helper) {
        var render = this.superAfterRender();
        helper.onRender(component);
        return render;
    }

})