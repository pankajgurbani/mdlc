({
    afterRender: function (component, helper) {
        this.superAfterRender();
        helper.initiateMDC(component);
    }
});