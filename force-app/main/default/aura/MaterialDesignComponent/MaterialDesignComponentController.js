({
    afterScriptsLoaded : function(component, event, helper) {
        if(mdc) {
            helper.initiateMDC(component);
        }
    },
    onFocus: function(component, event, helper) {
        var mdc = component.find('mdc-cmp');
        if(!mdc || !mdc.focus) return;
        if(Array.isArray(mdc)) {
            for(const cmp in mdc) {
                cmp.focus();
            }
        } else {
         	mdc.focus();   
        }
    },
    onclick: function(component, event, helper) {
        helper.fireMDCEvent(component, event, 'onclick', 'click');
    }
})