({
  initiateMDC: function (component) {
    try {
      mdc.autoInit();
      component.set("v.isLoadedMaterial", true);
    } catch (err) {}
  },
  fireMDCEvent: function (component, event, evtName, type) {
    var mdcEvent = component.getEvent(evtName);
    if (!mdcEvent) return;
    mdcEvent.setParam("type", type);
    mdcEvent.setParam("event", event);
    mdcEvent.fire();
  },
  getComponentElement: function (component, auraId) {
    var cmp = component.find(auraId);
    if (!cmp || !cmp.getElement) return;
    return cmp.getElement();
  }
});