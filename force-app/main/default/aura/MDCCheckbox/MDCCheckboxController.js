({
    onchange : function(component, event, helper) {
        var selected = component.get('v.selected');
        var checked = !component.get('v.checked');
        component.set('v.checked', checked);

        if(component.get('v.isGrouped') == true) {
            if(!Array.isArray(selected)) selected = [];
            var value = component.get('v.value');
            if(!checked) {
                if(selected.includes(value)) {
                    selected = selected.filter(val => String(val) !== String(value));
                }
            } else {
                if(!selected.includes(value)) {
                    selected.push(value);
                }
            }
        } else {
            selected = checked;
        }
        component.set('v.selected', selected);
    }
})