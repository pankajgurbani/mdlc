({
	onReportValidity : function (component, event, helper) {
		var mdcmp = component.find('mdc-cmp');
		if(!mdcmp) return;
		mdcmp = mdcmp.getElement();
		return mdcmp.reportValidity();
	}
})