({
    onReportValidity : function(component, event, helper) {
		return helper.isChoicesValid(component);
	}
})