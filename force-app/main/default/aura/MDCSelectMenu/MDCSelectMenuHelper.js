({
    instantiate : function(component) {
        var mdc_select = component.find('mdc-select');
        if(!mdc_select) return;
        try {
            var mdcmp = mdc.select.MDCSelect.attachTo(mdc_select.getElement());
            component.set('v.mdcmp', mdcmp);
            mdcmp.listen('MDCSelect:change', $A.getCallback(() => {
                component.set('v.value', mdcmp.value);
                this.reportValidity(component);
            }));
            var helpertext = component.find('mdc-select-helper');
            if(helpertext) {
                mdc.select.MDCSelectHelperText.attachTo(helpertext.getElement());
            }
            this.apiControls(component);
            if(component.get('v.value')) {
                this.setValue(component);
            }
        } catch(err) {}
    },

    apiControls: function(component) {
        try {
            var mdcmp = component.get('v.mdcmp');
            if(!mdcmp) return;
            var disabled = component.get('v.disabled');
            mdcmp.disabled = disabled;
            mdcmp.required = required;
        } catch(err) {}
    },

    setValue: function(component) {
        try {
            var mdcmp = component.get('v.mdcmp');
            if(!mdcmp) return;
            var value = component.get('v.value');
            if(mdcmp.value !== value) {
                mdcmp.value = value;
            }
        } catch(err) {}
    },

    reportValidity: function(component) {
        var mdcmp = component.get('v.mdcmp');
        var msg = component.get('v.messageWhenValueMissing');
        if(mdcmp) {
            mdcmp.valid && (msg = '');
            mdcmp.foundation && mdcmp.foundation.setValid && mdcmp.foundation.setValid(mdcmp.valid);
            !mdcmp.valid && mdcmp.root && mdcmp.root.focus && mdcmp.root.focus();
        }
        component.set('v.validationMessage', msg);
        return !msg;
    }
})