({

    afterRender: function(component, helper) {
        var render = this.superAfterRender();
        helper.apiControls(component);
        return render;
    },

    rerender: function(component, helper) {
        var render = this.superRerender();
        helper.apiControls(component);
        return render;
    }

})