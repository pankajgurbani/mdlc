({
    onInstantiate : function(component, event, helper) {
        helper.instantiate(component);
    },
    onReportValidity: function(component, event, helper) {
        return helper.reportValidity(component);
    },
    onblur: function(component, event, helper) {
        helper.reportValidity(component);
    },
    onchange: function(component, event, helper) {
        helper.setValue(component);
    }
})