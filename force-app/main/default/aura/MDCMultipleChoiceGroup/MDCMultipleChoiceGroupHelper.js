({
	isChoicesValid : function (component) {
		var value = component.get('v.value');
		var required = component.get('v.required');
        var valMsg, valid = true;
        if(required && $A.util.isEmpty(value)) {
            valid = false;
            valMsg = component.get('v.messageWhenValueMissing');
        }
		component.set('v.validationMessage', valMsg);
		return valid;
	}
})