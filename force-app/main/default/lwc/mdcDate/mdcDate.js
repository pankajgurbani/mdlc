import { LightningElement, api, track } from "lwc";
import { loadStyle, loadScript } from "lightning/platformResourceLoader";

import MaterialDesign from "@salesforce/resourceUrl/MaterialDesign";

export default class MdcDate extends LightningElement {
  @api label;
  @api required = false;
  @api value;
  @api helptext;
  @api min;
  @api max;
  @api disabled = false;
  @api validationMessage;

  @track message;
  @track valid;

  connectedCallback() {
    this.message = this.helptext;
    Promise.all([
      loadStyle(this, MaterialDesign + "/material.min.css"),
      loadScript(this, MaterialDesign + "/material.min.js")
    ]).then(() => {
      this.initiateMDC();
    });
  }

  @api
  setCustomValidity(msg) {
      let helperText = this.template.querySelector('.mdc-text-field-helper-text');
      if(!helperText) return;
      if(msg) {
        helperText.classList.remove('mdc-text-field-helper-text--validation-msg');
        helperText.classList.add('mdc-text-field-helper-text--validation-msg');
      } else {
        helperText.classList.remove('mdc-text-field-helper-text--validation-msg');
      }
      this.message = msg;
  }

  onblur(e) {
    let target = this.template.querySelector(".mdc-text-field__input");
    if(target) {
        const blurEvent = new CustomEvent('blur', {
            detail : {target}
        });
        this.dispatchEvent(blurEvent);
    }
  }

  @api
  reportValidity() {
    let target = this.template.querySelector(".mdc-text-field__input");
    if(target) {
        if(!target.validity.valid) {
            this.displayValidity(target);
        }
        return target.validity.valid;
    }
  }

  displayValidity(target) {
    target.focus();
    target.blur();
    target.focus();
  }

  oninput(e) {
    let date = this.template.querySelector(".mdc-text-field__input").value;
    const changeEvent = new CustomEvent("change", {
      detail: { value: date }
    });
    this.dispatchEvent(changeEvent);
  }

  initiateMDC() {
    try {
      mdc.textField.MDCTextField.attachTo(this.template.querySelector(".mdc-text-field"));
    } catch (err) {}
  }

  renderedCallback() {
    this.initiateMDC();
  }
}